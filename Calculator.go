package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	// Создаем новый сканер для чтения ввода с консоли
	scanner := bufio.NewScanner(os.Stdin)

	// Ожидаем ввод строки с пользовательским запросом
	fmt.Println("Введите математическое выражение (например, '1 + 2'): ")

	// Считываем строку из консоли
	scanner.Scan()
	input := scanner.Text()

	// Разбиваем строку на отдельные элементы
	args := strings.Split(input, " ")
	if len(args) != 3 {
		panic("Выдача паники, так как формат математической операции не удовлетворяет заданию")
	}

	// Получаем числа и оператор
	aStr := args[0]
	operator := args[1]
	bStr := args[2]

	// Преобразуем строковые числа в числовые значения
	a := parseNumber(aStr)
	b := parseNumber(bStr)

	// Проверяем, что оба числа имеют одинаковый тип (либо арабские, либо римские)
	if (isArabic(a) && isArabic(b)) || (isRoman(a) && isRoman(b)) {
		// Выполняем операцию в зависимости от оператора
		var result int
		switch operator {
		case "+":
			result = a + b
		case "-":
			result = a - b
		case "*":
			result = a * b
		case "/":
			if b == 0 {
				panic("Деление на ноль")
			}
			result = a / b
		default:
			panic("Выдача паники, так как строка не является математической операцией")
		}

		// Выводим результат
		fmt.Println("Результат:", result)
	} else {
		// Если числа разного типа, выдаем панику
		panic("Выдача паники, так как используются одновременно разные системы счисления")
	}
}

// Функция для преобразования строки в число
func parseNumber(numStr string) int {
	arabicNumbers := map[string]int{
		"I":    1,
		"II":   2,
		"III":  3,
		"IV":   4,
		"V":    5,
		"VI":   6,
		"VII":  7,
		"VIII": 8,
		"IX":   9,
		"X":    10,
	}

	if val, ok := arabicNumbers[numStr]; ok {
		return val
	}

	return atoi(numStr)
}

// Функция для проверки, является ли число арабским
func isArabic(num int) bool {
	return num >= 1 && num <= 10
}

// Функция для проверки, является ли число римским
func isRoman(num int) bool {
	return num >= 1
}

// Функция для преобразования строки в арабское число
func atoi(numStr string) int {
	num, err := strconv.Atoi(numStr)
	if err != nil {
		panic("Ошибка преобразования строки в число")
	}
	if num < 1 || num > 10 {
		panic("Выдача паники, так как число не входит в допустимый диапазон")
	}
	return num
}
